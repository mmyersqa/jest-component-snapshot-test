import React from 'react';
import Link from './Link.react';
import renderer from 'react-test-renderer';

test('Link changes the class when hovered', () => {
  const component = renderer.create(
    <Link page="http://www.google.com">Google</Link>,
  );

  // Initializing tree render
  let tree = component.toJSON();

  // Compare render to snapshot
  expect(tree).toMatchSnapshot();

  // Manually trigger the onMouseEnter callback
  tree.props.onMouseEnter();

  // Re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // Manually trigger the onMouseLeave callback
  tree.props.onMouseLeave();

  // Re-rendering
  tree = component.toJSON();

  // We're comparing the rendered output to the previous snapshot
  expect(tree).toMatchSnapshot();

});
